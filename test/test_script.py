import os
#import signal
#import subprocess
from os import environ
from pathlib import Path
from time import sleep

import pytest
from epics import PV, ca
from run_iocsh import IOC

os.environ["IOCNAME"] = "TEST-NSO-BProd:SC-IOC-001"


class beamProdTestHarness:
    def __init__(self):
        # timeout value in seconds for pvput/pvget calls
        self.timeout = 5
        self.putTimeout = self.timeout
        self.getTimeout = self.timeout

# Standard test fixture
@pytest.fixture(scope="function")
def test_inst():
    """
    Instantiate test harness
    """
    # Initialize channel access
    ca.initialize_libca()

    # Create handle to Test Harness
    test_inst = beamProdTestHarness()

    # Drop to test
    yield test_inst

    # Shut down channel access
    ca.flush_io()
    ca.clear_cache()


@pytest.fixture(scope="function")
def test_ioc():
    """
    Instantiate a test IOC.

    Start it, Yield it to the test function, shut it down.
    """
    env_vars = ["EPICS_BASE", "E3_REQUIRE_VERSION", "TEMP_CELL_PATH"]
    base, require, cell_path = map(environ.get, env_vars)
    if not cell_path:
        cell_path = Path(__file__).parent.parent / "cellMods"
    else:
        cell_path = Path(cell_path)
    assert cell_path.is_dir()

    assert base
    assert require

    iocsh_path = Path(base) / "require" / require / "bin" / "iocsh"
    assert iocsh_path.is_file()

    test_cmd = Path(__file__).parent / "st_test.cmd"
    assert test_cmd.is_file()

    args = ["-l", cell_path, test_cmd]
    ioc = IOC(*args, ioc_executable=iocsh_path, timeout=20.0)
    ioc.start()
    assert ioc.is_running()

    ioc.sleep_time = 3.0
    sleep(ioc.sleep_time)

    yield ioc

    ioc.exit()
    assert not ioc.is_running()


class TestUnitTests:
    def test_001(self, test_inst, test_ioc):
        """
        Start the IOC.
        Read value
        """
        pvName = "TEST_BPROD:TEST_Ops:BDestination"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue == 10
        assert pv.severity == 0

    def test_002(self, test_inst, test_ioc):
        """
        Start the server, start the IOC.
        Read value
        """
        pvName = "TEST_BPROD:TEST_Ops:BMode"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue == 0
        assert pv.severity == 0
