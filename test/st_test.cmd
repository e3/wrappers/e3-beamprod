# This should be a test or example startup script
require beamprod

iocshLoad("$(beamprod_DIR)/beamprod.iocsh", "P=TEST_BPROD, R=TEST_Ops, PP=NaN, LEBT_IRIS=TST_IRIS, MAGNETRON=TST_MAG, MEBT_CHP=TST_CHOPP, BCM_CRATE=TST_BCM")

# always leave a blank line at EOF
