
epicsEnvSet("TOP","$(E3_CMD_TOP)/..")
epicsEnvSet("APP", "$(TOP)/beamprod/beamprodApp")

dbLoadRecords("$(APP)/Db/beamprod-timing.template","P=BPROD, R=Ops, PP=TD-M:Ctrl-SCE-1, LEBT_IRIS=LEBT-Iris:ID-Iris-01, MAGNETRON=ISrc-CS:ISS-Magtr-01, MEBT_CHP=MEBT-010:BMD-Chop-001")
dbLoadRecords("$(APP)/Db/beamprod-mode-dest.template","P=BPROD, R=Ops, PP=TD-M:Ctrl-SCE-1, LEBT_IRIS=LEBT-Iris:ID-Iris-01, MAGNETRON=ISrc-CS:ISS-Magtr-01, MEBT_CHP=MEBT-010:BMD-Chop-001")
dbLoadRecords("$(APP)/Db/beamprod-olc.template","P=BPROD, R=Ops, PP=TD-M:Ctrl-SCE-1, LEBT_IRIS=LEBT-Iris:ID-Iris-01, MAGNETRON=ISrc-CS:ISS-Magtr-01, MEBT_CHP=MEBT-010:BMD-Chop-001")
dbLoadRecords("beamprod-current.template","P=$(P), R=$(R), PP=$(PP), LEBT_IRIS=$(LEBT_IRIS), MAGNETRON=$(MAGNETRON), MEBT_CHP=$(MEBT_CHP)")

# always leave a blank line at EOF

