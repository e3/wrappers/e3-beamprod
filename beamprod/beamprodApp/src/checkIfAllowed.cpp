/* checkIfAllowedImpl.cpp */
/* Author: Arek Gorzawski */
/* Date:    2021-03-18 */
#include <string.h>
#include <stdlib.h>
#include <registryFunction.h>
#include <aSubRecord.h>
#include <menuFtype.h>
#include <errlog.h>
#include <epicsString.h>
#include <epicsExport.h>

#include <string>
#include <vector>
#include <sstream>
#include <iostream>

#include "essLinacConfigFunctions.h"

using namespace std;


long check_if_allowed(aSubRecord *prec)
{
    const char* beamMode = (const char*)(prec->a);
    const char* beamDestination = (const char*)(prec->b);
    return check_allowed_mode_for_destination(beamMode, beamDestination);

}

long check_if_allowed_table(aSubRecord *prec)
{
    const char* beamMode = (const char*)(prec->a);
    const float beamFrequency = *(const float*)(prec->b);
    const float beamPulseLength = *(const float*)(prec->c);

    return check_pulse_length_for_mode(beamMode, beamPulseLength) * check_frequency_for_mode(beamMode, beamFrequency);
}



extern "C" {
epicsRegisterFunction(check_if_allowed);
epicsRegisterFunction(check_if_allowed_table);
}
