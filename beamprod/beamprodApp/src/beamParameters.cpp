/* beamParametersImpl.cpp */
/* Author: Arek Gorzawski */
/* Date:    2021-03-24 */
#include <string.h>
#include <stdlib.h>
#include <registryFunction.h>
#include <aSubRecord.h>
#include <menuFtype.h>
#include <errlog.h>
#include <epicsString.h>
#include <epicsExport.h>

#include <string>
#include <vector>
#include <sstream>
#include <iostream>

#include "essLinacConfigFunctions.h"

using namespace std;

long get_beam_parameters(aSubRecord *prec)
{
    const char* beamMode = (const char*)(prec->a);
    *(double *) prec->valb = requestParametersMap[beamMode].beamCurrentInmA;
    *(double *) prec->vala = requestParametersMap[beamMode].pulseLengthInUs;
    *(double *) prec->valc = requestParametersMap[beamMode].beamFrequency;
    *(double *) prec->vald = requestParametersMap[beamMode].integratedChargeInUC;
    *(double *) prec->vale = requestParametersMap[beamMode].averageCurrentInUA;
    return 0;
}

extern "C" {
epicsRegisterFunction(get_beam_parameters);
}