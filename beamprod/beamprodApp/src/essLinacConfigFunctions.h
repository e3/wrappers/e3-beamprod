/* Author: Arek Gorzawski */
/* Date:    2021-03-18 */
#include <map>
#include <string>
#include <algorithm>

using namespace std;

#include "essLinacConfig.h"

static int check_pulse_length_for_mode(string beamMode, float pulseL)
{
    // TODO add map check key
    if (pulseL <= requestParametersMap[beamMode].pulseLengthInUs)
        return 1;
    return 0;
}

static int check_beam_current_for_mode(string beamMode, float current)
{
    // TODO add map check key
    if (current <= requestParametersMap[beamMode].beamCurrentInmA)
        return 1;
    return 0;
}

static int check_frequency_for_mode(string beamMode, float frequency)
{
    // TODO add map check key
    if (frequency <= requestParametersMap[beamMode].beamFrequency)
        return 1;
    return 0;
}


static int check_allowed_mode_for_destination(string beamMode, string beamDestination)
{
    string *foo = std::find(std::begin(requestTypesMap[beamDestination].allowed), std::end(requestTypesMap[beamDestination].allowed), beamMode);
    if (foo != std::end(requestTypesMap[beamDestination].allowed)) {
        return 1;
    } else {
        return 0;
    }
    return -1;
}
