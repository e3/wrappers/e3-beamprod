/* Author: Arek Gorzawski */
/* Date:    2022-12-10 */
#include <string.h>
#include <stdlib.h>
#include <registryFunction.h>
#include <aSubRecord.h>
#include <menuFtype.h>
#include <errlog.h>
#include <epicsString.h>
#include <epicsExport.h>

#include <string>
#include <vector>
#include <sstream>
#include <iostream>

#include "essLinacConfigFunctions.h"

using namespace std;

long set_iris_for_current(aSubRecord *prec)
{
    double expected_current = *(epicsFloat64*)(prec->a);
    double expected_current_previous = *(epicsFloat64*)(prec->ovla);
    const char* beam_modeC = (const char*)(prec->c);
    std::string beam_mode(beam_modeC);
//    std::cout << "Updating to set CURRENT[mA] (new) to " << expected_current << " from (old) " << expected_current_previous << "\n";
    *(double *) prec->valb = check_beam_current_for_mode(beam_mode, expected_current);
    if (expected_current != expected_current_previous){
        int iris_mm = 0;
        double *iris_current_profile = (double*)(prec->b);
//        std::cout << "Size " << prec->nob << "\n";
//        std::cout << "Waveform " << iris_current_profile << "\n";
        for (int i=0; i < prec->nob; i++) {
//            std::cout << "iris-map-current " << i << " - " << iris_current_profile[i] << "\n";
            if (iris_current_profile[i] > expected_current) {
                break;
            }
            iris_mm = i; // TODO first implementation returns the IRIS opening quantified to 1mm
        }
//        std::cout << "Updating to set IRIS[mm] to " << iris_mm << "\n";
        *(double *) prec->vala = iris_mm;
    }
    return 0;
}

extern "C" {
epicsRegisterFunction(set_iris_for_current);
}