// on: 2025-02-07 13:31:52.371171
// using python script
// auto generated using: https://gitlab.esss.lu.se/icshwi/reftabs/-/raw/master/init/beamallowed_commissioning_dumpline.yml?inline=false
// auto generated using: https://gitlab.esss.lu.se/icshwi/reftabs/-/raw/master/init/beamcurrent_allowed.yml?inline=false

#include <map>
#include <string>
using namespace std;

struct AllowedModes
{
    string allowed[20];
};

struct AllowedParameters
{
    float beamCurrentInmA;
    float pulseLengthInUs;
    float beamFrequency;
    float integratedChargeInUC;
    float averageCurrentInUA;
};

static map<int, string> beamModes = {
    // {EPICS order,  BeamName} as used in mbbo selection combos defined in DB templates
    
    { 1 , "NoBeam"},
    { 10 , "Conditioning"},
    { 20 , "Probe"},
    { 30 , "FastCommissioning"},
    { 40 , "RfTest"},
    { 50 , "StabilityTest"},
    { 60 , "SlowCommissioning"},
    { 70 , "FastTuning"},
    { 80 , "SlowTuning"},
    { 90 , "LongPulseVerification"},
    { 100 , "ShieldingVerification"},
    { 110 , "Production"},
};

static map<int, string> beamDestinations = {
    // {EPICS order,  BeamDestination} as used in mbbo selection combos defined in DB templates

    { 10 , "Isrc"},
    { 20 , "LebtFc"},
    { 30 , "MebtFc"},
    { 40 , "Dtl2Fc"},
    { 50 , "Dtl4Fc"},
    { 60 , "SpkIbs"},
    { 70 , "MblIbs"},
    { 80 , "BeamDump"},
    { 90 , "Target"},
};

static map<string, AllowedModes> requestTypesMap = {
    // {BeamDestination,  {allowedBeamModes}}
    
    {"Isrc", { "Conditioning", }},
    {"LebtFc", { "NoBeam", "Probe", "FastCommissioning", "RfTest", "StabilityTest", "SlowCommissioning", "FastTuning", "SlowTuning", "LongPulseVerification", "ShieldingVerification", "Production", }},
    {"MebtFc", { "NoBeam", "Probe", "FastCommissioning", "RfTest", "SlowCommissioning", "FastTuning", "SlowTuning", }},
    {"Dtl2Fc", { "NoBeam", "Probe", "FastCommissioning", "RfTest", "SlowCommissioning", "FastTuning", "SlowTuning", }},
    {"Dtl4Fc", { "NoBeam", "Probe", }},
    {"SpkIbs", { "NoBeam", "Probe", }},
    {"MblIbs", { "NoBeam", "Probe", }},
    {"BeamDump", { "NoBeam", "Probe", "FastCommissioning", "RfTest", "StabilityTest", "SlowCommissioning", "FastTuning", "SlowTuning", "LongPulseVerification", }},
    {"Target", { "NoBeam", }},
};

static map<string, AllowedParameters> requestParametersMap = {
    // {BeamMode,  {pulseLengthInUs, beamCurrentInUA, beamFrequency, integratedChargeInUC, averageCurrentInUA}}
    
    //{"units", { mA, us, Hz, uC, uA, }},
    {"NoBeam", { 0, 0, 0, 0, 0, }},
    {"Conditioning", { 0, 0, 0, 0, 0, }},
    {"Probe", { 6, 5, 1, 0.03, 0.03, }},
    {"FastCommissioning", { 6, 5, 14, 0.03, 0.42, }},
    {"RfTest", { 6, 50, 1, 0.3, 0.3, }},
    {"StabilityTest", { 6, 50, 14, 0.3, 4.2, }},
    {"SlowCommissioning", { 62.5, 5, 1, 0.31, 0.31, }},
    {"FastTuning", { 62.5, 5, 14, 0.31, 4.37, }},
    {"SlowTuning", { 62.5, 50, 1, 3.13, 3.13, }},
    {"LongPulseVerification", { 62.5, 2860, 0.0333, -1, -1, }},
    {"ShieldingVerification", { 0, 0, 0, -1, -1, }},
    {"Production", { 62.5, 2860, 14, -1, -1, }},
};
