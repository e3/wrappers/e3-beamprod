"""
Generator script for beam mode/parameters static imports for the cpp header

Arek Gorzawski 2021, ESS
"""

from jinja2 import Environment, FileSystemLoader
import datetime
import yaml
import re
import requests

template_file_name = 'essLinacConfig.h.template'
usedSources = []


def download_and_save(input_yaml):
    print('=====> ', input_yaml)
    urlToLatest = 'https://gitlab.esss.lu.se/icshwi/reftabs/-/raw/master/init/{}?inline=false'.format(input_yaml)
    print('Downloading the latest {}'.format(input_yaml))
    print('Source: {}'.format(urlToLatest))
    r = requests.get(urlToLatest, allow_redirects=True)
    open(input_yaml, 'wb').write(r.content)
    usedSources.append(urlToLatest)

input_yaml = r'beamallowed_commissioning_dumpline.yml'
download_and_save(input_yaml)
beammodes = {}
with open(input_yaml) as file:
    for item, doc in yaml.full_load(file).items():
        if item == 'PBDest':
            for one in doc:
                beammodes[one] = [e for e in doc[one]]
                #print(doc[one])


input_yaml = r'beamcurrent_allowed.yml'
download_and_save(input_yaml)
beammodesParameters = {}
with open(input_yaml) as file:
    for item, doc in yaml.full_load(file).items():
        if item == 'doc' or item == 'source' or item == 'desc':
            continue
        beammodesParameters[item]= list(doc.values())

#input_yaml = r'databuffer-ess.yml'
#download_and_save(input_yaml)
#result = {'BMod': [], 'BDest': []}
#with open(input_yaml) as file:
#    for item, doc in yaml.full_load(file).items():
#        if item in list(result.keys()):
#            for indx, one in enumerate(doc.keys()):
#                result[item].append([indx, one])

# populate the template and save the output
file_loader = FileSystemLoader('.')
env = Environment(loader=file_loader)
template = env.get_template(template_file_name.format(""))
output = template.render(date=datetime.datetime.now(),
                         sources=usedSources,
#                         beammodesIndexed=result['BMod'],
#                         beamDestinationsIndexed=result['BDest'],
                         beammodes=beammodes,
                         beammodesParameters = beammodesParameters)
with open(template_file_name.replace('.template',''), "w") as fh:
    fh.write(output)
print('=> done, saved to:  ', template_file_name.replace('.template',''))
