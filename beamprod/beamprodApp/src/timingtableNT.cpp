/**/

#include "timingtableNT.h"
#include <iostream>
#include <algorithm>

#include <epicsEvent.h>
#include <initHooks.h>
#include <iocsh.h>
#include <epicsExport.h>
#include <epicsExit.h>

#include <pvxs/server.h>
#include <pvxs/sharedpv.h>
#include <pvxs/nt.h>
#include <pvxs/log.h>
#include <pvxs/iochooks.h>
#include <pvxs/client.h>

#include "essLinacConfigFunctions.h"


//TODO: Maybe add a verification if the column number exists on the NTTable
float TimingTableNTManager::getEventPosition(pvxs::Value table, std::string eventName, size_t line,  bool inputSC){
    if (m_EventIdxDict.count(eventName) > 0){
        if (inputSC)
            return (table["Col" + std::to_string(m_EventIdxDict[eventName])].as<pvxs::shared_array<const float>>())[line];
        else
            return (table["value"]["Col" + std::to_string(m_EventIdxDict[eventName])].as<pvxs::shared_array<const float>>())[line];
    }
    std::cout << "Event not found: " << eventName << std::endl;
    return -1;
}

// Prepare the superCycle table including the labels and metadata
void TimingTableNTManager::prepareTable(pvxs::Value supercycleIn, pvxs::Value listEvents){
        pvxs::Value supercycleInValue(supercycleIn["value"]);

        size_t sizeEventCodes = listEvents["value"]["codes"].as<pvxs::shared_array<const uint8_t>>().size();
        pvxs::shared_array<uint8_t> codes = listEvents["value"]["codes"].as<pvxs::shared_array<const uint8_t>>().thaw();
        pvxs::shared_array<std::string> names = listEvents["value"]["names"].as<pvxs::shared_array<const std::string>>().thaw();

        std::vector<std::string> labels;
        uint8_t labelCode;
        std::string fieldNameIn, fieldNameOut;
        //TODO: for some reason not defining a big initial size is breaking the IOC when
        //the number of lines is bigger than 13
        std::vector<float> col(1000);
        // generate the id column
        fieldNameIn = "Col0";

        //Check if all columns have the same size
        m_ColLen = (supercycleInValue[fieldNameIn].as<pvxs::shared_array<const float>>()).size();
        for (size_t i = 0; i < maxCols; i++) {
            fieldNameIn = "Col" + std::to_string(i);
            if (m_ColLen != supercycleInValue[fieldNameIn].as<pvxs::shared_array<const float>>().size())
                std::cout << "Column with invalid lengh" << fieldNameIn << std::endl;
        }

        // Insert the Id column
        labels.insert(labels.end(), "Id");
        col.resize(m_ColLen-1);
        for (size_t k = 0; k < m_ColLen; k++)
            col[k] = k;
        m_OutTable["value"]["Col0"] = pvxs::shared_array<float const>(col.begin(), col.end());

        // Reset event positions 
        m_EventIdxDict.clear();
        for (size_t i = 0; i < sizeEventCodes; i++)
            m_EventIdxDict[names[i]] = -1;
      
        for (size_t i = 0; i < maxCols; i++) {
            fieldNameIn = "Col" + std::to_string(i);
            fieldNameOut = "Col" + std::to_string(i+1);
            labelCode = supercycleInValue[fieldNameIn].as<pvxs::shared_array<const float>>()[0];
            if (labelCode != -1 && labelCode != 0){
                bool foundCode = false;
                for (size_t j = 0; j < sizeEventCodes; j++)
                    if (codes[j] == labelCode){
                        foundCode = true;
                        labels.insert(labels.end(),names[j]);
                        m_EventIdxDict[names[j]] = i;
                    }
                // code not found
                if (not foundCode){
                    // If Beam Length is not present, include it
                    if (m_EventIdxDict.count("BLen") == 0 || m_EventIdxDict["BLen"] < 0){ //labelCode <= 0
                        m_EventIdxDict["BLen"] = i;
                        // Insert Beam Length column
                        labels.insert(labels.end(),"BLen");
                    } else { // if RfPiezoSt is not present, include it
                        if (m_EventIdxDict["RfPiezoSt"] < 0){ //labelCode <= 0
                            m_EventIdxDict["RfPiezoSt"] = i;
                            // Insert RfPiezoSt column
                            labels.insert(labels.end(),"RfPiezoSt");
                        } else // if BPulseStPrev is not present, include it
                            if (m_EventIdxDict["BPulseStPrev"] < 0){ //labelCode <= 0
                                m_EventIdxDict["BPulseStPrev"] = i;
                                // Insert BPulseStPrev column
                                labels.insert(labels.end(),"BPulseStPrev");
                            }
                    }
                }
            }
            // save the data    
            for (size_t k=1; k < m_ColLen ; k++ ){
                col[k-1] = (supercycleInValue[fieldNameIn].as<pvxs::shared_array<const float>>())[k];
            }

            m_OutTable["value"][fieldNameOut] = pvxs::shared_array<float const>(col.begin(), col.end());
        }

        //Insert automatically RF Piezo Start
        float rfPiezoStPos, rfStPos;
        //TODO: is this check needed ?
        if (m_EventIdxDict["RfSt"] > -1){
            for (size_t k=1; k < m_ColLen ; k++ ){
                rfStPos = getEventPosition(supercycleInValue, "RfSt", k, true);
                if (rfStPos > 0)
                    rfPiezoStPos = rfStPos - m_RfPiezoStDelaySp["value"].as<float>();
                else
                    rfPiezoStPos = -1;
                col[k-1] = rfPiezoStPos;
            }
            m_OutTable["value"]["Col" + std::to_string(m_EventIdxDict["RfPiezoSt"]+1)] = pvxs::shared_array<float const>(col.begin(), col.end());
        }

        //Insert automatically BPulseStPrev
        if (m_EventIdxDict["BPulseSt"] > -1){
            float bPulseStPos, bPulseStPrevPos;
            //TODO: remove hardcoded delay for BPulseStPrev
            float delayBPulseStPrev = m_BPulseStPrevDelaySp["value"].as<float>();
            for (size_t k=1; k < m_ColLen ; k++ ){
                bPulseStPos = getEventPosition(supercycleInValue, "BPulseSt", k, true);
                if (bPulseStPos > 0 && (delayBPulseStPrev-bPulseStPos > 0))
                    bPulseStPrevPos = ((float)1/14*1000000)-(delayBPulseStPrev-bPulseStPos);
                else
                    bPulseStPrevPos = -1;
                if (k > 1 && k < m_ColLen)
                    col[k-2] = bPulseStPrevPos;
                else if ( k == 1 ) // if first row, the previous row is the latest
                    col[m_ColLen-2] = bPulseStPrevPos;
            }
            m_OutTable["value"]["Col" + std::to_string(m_EventIdxDict["BPulseStPrev"]+1)] = pvxs::shared_array<float const>(col.begin(), col.end());
        }

        // bPulsePos is the position of the Beam withing the BeamWindow
        float maxBPulseLengthSrc = 0, maxBPulseLengthLebt = 0, maxBPulseLength = 0, bPulseLen = 0, detectedFrequency =0, bPulsePos=beamWindowEnd, minPulsePos=beamWindowEnd;
        size_t cyclesWithBeam = 0;
        m_AllowChangeBPulseLen = true;
        //Looks for the maximum Beam Length and the minimum Beam Pos
        if (m_EventIdxDict["BPulseSt"] > -1 && m_EventIdxDict["BPulseEnd"] > -1){
            float beamStPos, beamEndPos, lebtCpOffPos, lebtCpOnPos, ionMagStPos, ionMagEndPos;
            for (size_t k=1; k < m_ColLen ; k++ ){
                // there is a beamStart
                beamStPos = getEventPosition(supercycleInValue, "BPulseSt", k, true);
                beamEndPos = getEventPosition(supercycleInValue, "BPulseEnd", k, true);
                lebtCpOnPos = getEventPosition(supercycleInValue, "LebtCpOn", k, true);
                lebtCpOffPos = getEventPosition(supercycleInValue, "LebtCpOff", k, true);
                ionMagStPos = getEventPosition(supercycleInValue, "IonMagSt", k, true);
                ionMagEndPos = getEventPosition(supercycleInValue, "IonMagEnd", k, true);

                //Save Beam Pulse Length and BeamPos
                if ( beamStPos > 0 && beamEndPos > 0){
                    bPulseLen = beamEndPos-beamStPos;
                    bPulsePos = beamStPos - beamWindowSt;
                }
                else{
                    bPulseLen = 0;
                    bPulsePos = beamWindowEnd;
                }

                col[k-1] = bPulseLen;

                if ( beamStPos > 0 && beamEndPos > 0){
                    if (bPulseLen > maxBPulseLength){
                        //If there are different Pulse Len
                        if (maxBPulseLength > 0)
                            m_AllowChangeBPulseLen = false;
                        maxBPulseLength = bPulseLen;
                    }
                    if (bPulsePos < minPulsePos)
                        minPulsePos = bPulsePos;
                    if (lebtCpOnPos-lebtCpOffPos > maxBPulseLengthLebt)
                        maxBPulseLengthLebt = lebtCpOnPos-lebtCpOffPos;
                    if (ionMagEndPos-ionMagStPos > maxBPulseLengthSrc)
                        maxBPulseLengthSrc = ionMagEndPos-ionMagStPos;
                    cyclesWithBeam++;
                }
            }

            m_OutTable["value"]["Col" + std::to_string(m_EventIdxDict["BLen"]+1)] = pvxs::shared_array<float const>(col.begin(), col.end());
            m_BeamPos["value"] = minPulsePos;

        }
        // calculate detected Frequency
        detectedFrequency = (float)nominalNumberOfCycles  * (float) cyclesWithBeam / (float) (m_ColLen-1);

        m_OutTable["metadata"]["BPulseLengthSrc"] = (uint32_t) maxBPulseLengthSrc;
        m_OutTable["metadata"]["BPulseLengthLebt"] = (uint32_t) maxBPulseLengthLebt;
        m_OutTable["metadata"]["BPulseLength"] = (uint32_t) maxBPulseLength;
        m_OutTable["metadata"]["BPulseFrequency"] = detectedFrequency;
        m_OutTable["metadata"]["Comment"] = "";
        m_OutTable["metadata"]["Keywords"] = (pvxs::shared_array<const std::string>){""};

        // resize labels to include empty labels on the whole table
        labels.resize(maxCols+1);
        m_OutTable["labels"] = pvxs::shared_array<const std::string>(labels.begin(), labels.end());

        // Updating to reflect the length of the columns on m_OutTable
        m_ColLen--;

           //TODO: review, maybe use local variables and then update the object ones here
       // Updating to reflect the position on m_OutTable, as a new Id column was added
       for (const auto& e : m_EventIdxDict){
            if (m_EventIdxDict[e.first] > -1)
                m_EventIdxDict[e.first]++;
        }
}

void TimingTableNTManager::reloadTable(){
        auto ctxt(pvxs::client::Context::fromEnv());

        // Get the RefTab Events from Supercycle IOC
        pvxs::Value listEvents = ctxt.get(m_ListEventsPVName).exec()->wait(5.0);

        prepareTable(m_PvTimTbl, listEvents);

        m_BeamLength["value"] = m_OutTable["metadata"]["BPulseLength"].as<const uint32_t>();
        // update SharedPV cache
        m_BeamLengthPv.post(m_BeamLength);

        //Update Beam Pulse Position
        m_BeamPosPv.post(m_BeamPos);

        // Update SCE Table
        updateSCETable();

        m_TimTblPv.post(m_PvTimTbl);
}

//Get the current m_OutTable and set to the SuperCycleEngine Time table
void TimingTableNTManager::updateSCETable(){
        //TODO: move to a class property
        // Configure client using $EPICS_PVA_*
        auto ctxt(pvxs::client::Context::fromEnv());
        // Set the new table to the SuperCycle IOC
        auto opSet = ctxt.put(m_SCENTOutPVName)
                // provide present value to build() callback.
                .fetchPresent(true)
                .build([this](pvxs::Value&& current) -> pvxs::Value {
                    // allocate an empty container
                    auto toput(current.cloneEmpty());

                    // fill in .value.
                    // Assignment implicitly marks .value as changed
                    std::string fieldName;
                    for (size_t i = 0; i < maxCols+1; i++) {
                        fieldName = "Col" + std::to_string(i);
                        toput["value"][fieldName] =
                            m_OutTable["value"][fieldName].as<pvxs::shared_array<const float>>();
                    }

                    toput["labels"] =
                        m_OutTable["labels"].as<pvxs::shared_array<const std::string>>();
                
                    //copy metadata fields
                    toput["metadata"]["BPulseLength"] = m_OutTable["metadata"]["BPulseLength"].as<const uint32_t>();
                    toput["metadata"]["BPulseLengthSrc"] = m_OutTable["metadata"]["BPulseLengthSrc"].as<const uint32_t>();
                    toput["metadata"]["BPulseLengthLebt"] = m_OutTable["metadata"]["BPulseLengthLebt"].as<const uint32_t>();
                    toput["metadata"]["BPulseFrequency"] = m_OutTable["metadata"]["BPulseFrequency"].as<const float>();
                    toput["metadata"]["Comment"] = m_OutTable["metadata"]["Comment"].as<const std::string>();
                    toput["metadata"]["Keywords"] = m_OutTable["metadata"]["Keywords"].as<pvxs::shared_array<const std::string>>();
                    // return the container to be sent
                  return toput;
                })
                .exec();

        opSet->wait(5.0);
}

void TimingTableNTManager::changeBeam(float beamLength, float beamPos){
    // Beam position should be >= 0
    if (beamPos < 0)
        return;

    float deltaSt = beamPos - m_BeamPos["value"].as<float>();        
    float deltaEnd = deltaSt + beamLength - m_BeamLength["value"].as<float>();

    //Updating related events to beam
    float beamSt, beamEnd, currentBPulseLen;
    float mebtCpHead, mebtCpTail, lebtCpOff, lebtCpOn, ionMagEnd, ionMagSt, beamStAhead, beamEndAhead, lebtCpOffAhead, lebtCpOnAhead, beamStPrev;
    std::vector<float> colBPulseSt, colBPulseEnd, colLebtCpOff, colLebtCpOn, colMebtCpHead, colMebtCpTail, colBLen;
    std::vector<float> colBPulseStAhead, colBPulseEndAhead, colLebtCpOffAhead, colLebtCpOnAhead, colBPulseStPrev;
    bool validBeamLength = true;

    if (m_EventIdxDict["BPulseSt"] > -1 && m_EventIdxDict["BPulseEnd"] > -1){
        for (size_t k=0; k < m_ColLen; k++ ){
            // there is a beamStart
            beamSt = getEventPosition(m_OutTable, "BPulseSt", k, false);
            beamEnd = getEventPosition(m_OutTable, "BPulseEnd", k, false);
            lebtCpOn = getEventPosition(m_OutTable, "LebtCpOn", k, false);
            lebtCpOff = getEventPosition(m_OutTable, "LebtCpOff", k, false);
            mebtCpHead = getEventPosition(m_OutTable, "MebtCpHead", k, false);
            mebtCpTail = getEventPosition(m_OutTable, "MebtCpTail", k, false);
            ionMagEnd = getEventPosition(m_OutTable, "IonMagEnd", k, false);
            ionMagSt = getEventPosition(m_OutTable, "IonMagSt", k, false);
            beamStAhead = getEventPosition(m_OutTable, "BPulseStAhead", k, false);
            beamEndAhead = getEventPosition(m_OutTable, "BPulseEndAhead", k, false);
            lebtCpOnAhead = getEventPosition(m_OutTable, "LebtCpOnAhead", k, false);
            lebtCpOffAhead = getEventPosition(m_OutTable, "LebtCpOffAhead", k, false);
            //beamStPrev = getEventPosition(m_OutTable, "BPulseStPrev", k, false);
            //BeamStPrev is set 1 pulse before
            if (k == 0)
                beamStPrev = getEventPosition(m_OutTable, "BPulseStPrev", m_ColLen-1, false);
            else
                beamStPrev = getEventPosition(m_OutTable, "BPulseStPrev", k-1, false);
            currentBPulseLen = 0;

            // change the beam start and beam end only on the cycles they are present
            if ( beamSt > 0 && beamEnd > 0) {
                // The other relevant events should also be > 0
                if ( lebtCpOn > 0 && ionMagEnd > 0) {
                    // only change beam start and beam end if MebtCpTail will
                    // not overpass IonMagEnd
                    if (lebtCpOn + deltaEnd > ionMagEnd){
                        //TODO: include some proper alarm
                        std::cout << "Invalid Beam length or position. LebtCpOn will be after IonMagEnd" << std::endl;
                        validBeamLength = false;
                    }
                    else {
                        // The move beam will be appllied only if these 2 checks are valid:
                        // 1. LebtOff and LebtOn should be within the Magnetron window :
                        // LebftOff > MagPulseSt and  LebtOn<MagPulseEnd
                        // 2. BeamSt and BeamEnd should be within the BeamWindow
                        if (lebtCpOff + deltaSt < ionMagSt || lebtCpOn + deltaEnd > ionMagEnd) {
                            std::cout << "Invalid Beam Position or Length, lebtCpOff and LebtOn should be within Magnetron window" << std::endl;
                            validBeamLength = false;
                        }
                        else 
                            if (beamSt + deltaSt < beamWindowSt || beamEnd + deltaEnd > beamWindowEnd){
                                std::cout << "Invalid Beam Position or length, beamSt and beamEnd should be within Beam window" << std::endl;
                                validBeamLength = false;
                            }
                            else {
                                beamSt = beamSt + deltaSt;
                                lebtCpOff = lebtCpOff + deltaSt;
                                beamStAhead = beamStAhead + deltaSt;
                                lebtCpOffAhead = lebtCpOffAhead + deltaSt;
                                mebtCpHead = mebtCpHead + deltaSt;
                                if (mebtCpTail > 0)
                                    mebtCpTail = mebtCpTail + deltaEnd;
                                lebtCpOn = lebtCpOn + deltaEnd;
                                beamEnd = beamEnd + deltaEnd;
                                lebtCpOnAhead = lebtCpOnAhead + deltaEnd;
                                beamEndAhead = beamEndAhead + deltaEnd;
                                beamStPrev = beamStPrev + deltaSt;
                                               
                                currentBPulseLen = beamLength;
                            }

                    } 
                } else
                    // Shouldn't apply any change if the mandatory events
                    // are not defined
                    validBeamLength = false;
            }
            colBPulseSt.insert(colBPulseSt.end(), beamSt);
            colBPulseEnd.insert(colBPulseEnd.end(), beamEnd);
            colMebtCpHead.insert(colMebtCpHead.end(), mebtCpHead);
            colMebtCpTail.insert(colMebtCpTail.end(), mebtCpTail);
            colLebtCpOn.insert(colLebtCpOn.end(), lebtCpOn);
            colLebtCpOff.insert(colLebtCpOff.end(), lebtCpOff);
            colBLen.insert(colBLen.end(), currentBPulseLen);
            colBPulseStAhead.insert(colBPulseStAhead.end(), beamStAhead);
            colBPulseEndAhead.insert(colBPulseEndAhead.end(), beamEndAhead);
            colLebtCpOnAhead.insert(colLebtCpOnAhead.end(), lebtCpOnAhead);
            colLebtCpOffAhead.insert(colLebtCpOffAhead.end(), lebtCpOffAhead);
            colBPulseStPrev.insert(colBPulseStPrev.end(), beamStPrev);
        }
    }

    if (validBeamLength){
        m_OutTable["value"]["Col" + std::to_string(m_EventIdxDict["BPulseSt"])] = pvxs::shared_array<const float>(colBPulseSt.begin(), colBPulseSt.end());
        m_OutTable["value"]["Col" + std::to_string(m_EventIdxDict["BPulseEnd"])] = pvxs::shared_array<const float>(colBPulseEnd.begin(), colBPulseEnd.end());
        m_OutTable["value"]["Col" + std::to_string(m_EventIdxDict["MebtCpHead"])] = pvxs::shared_array<const float>(colMebtCpHead.begin(), colMebtCpHead.end());
        m_OutTable["value"]["Col" + std::to_string(m_EventIdxDict["MebtCpTail"])] = pvxs::shared_array<const float>(colMebtCpTail.begin(), colMebtCpTail.end());
        m_OutTable["value"]["Col" + std::to_string(m_EventIdxDict["LebtCpOff"])] = pvxs::shared_array<const float>(colLebtCpOff.begin(), colLebtCpOff.end());
        m_OutTable["value"]["Col" + std::to_string(m_EventIdxDict["LebtCpOn"])] = pvxs::shared_array<const float>(colLebtCpOn.begin(), colLebtCpOn.end());
        m_OutTable["value"]["Col" + std::to_string(m_EventIdxDict["BPulseStAhead"])] = pvxs::shared_array<const float>(colBPulseStAhead.begin(), colBPulseStAhead.end());
        m_OutTable["value"]["Col" + std::to_string(m_EventIdxDict["BPulseEndAhead"])] = pvxs::shared_array<const float>(colBPulseEndAhead.begin(), colBPulseEndAhead.end());
        m_OutTable["value"]["Col" + std::to_string(m_EventIdxDict["LebtCpOffAhead"])] = pvxs::shared_array<const float>(colLebtCpOffAhead.begin(), colLebtCpOffAhead.end());
        m_OutTable["value"]["Col" + std::to_string(m_EventIdxDict["LebtCpOnAhead"])] = pvxs::shared_array<const float>(colLebtCpOnAhead.begin(), colLebtCpOnAhead.end());
        // Before inserting BPulseStPrev, rotate it
        std::rotate(colBPulseStPrev.begin(), colBPulseStPrev.begin()+1,colBPulseStPrev.end());
        m_OutTable["value"]["Col" + std::to_string(m_EventIdxDict["BPulseStPrev"])] = pvxs::shared_array<const float>(colBPulseStPrev.begin(), colBPulseStPrev.end());
        m_OutTable["value"]["Col" + std::to_string(m_EventIdxDict["BLen"])] = pvxs::shared_array<const float>(colBLen.begin(), colBLen.end());


        // Update beam Length metadata
        m_OutTable["metadata"]["BPulseLength"] = (uint32_t) beamLength;
        m_OutTable["metadata"]["BPulseLengthLebt"] = (uint32_t) m_OutTable["metadata"]["BPulseLengthLebt"].as<const uint32_t>() + (deltaEnd-deltaSt);

        updateSCETable();

        // update Beam Length Readback PV 
        m_BeamLength["value"] = beamLength;
        m_BeamLengthPv.post(m_BeamLength);

        // update Beam Pos Readback PV 
        m_BeamPos["value"] = beamPos;
        m_BeamPosPv.post(m_BeamPos);
    }
}

void TimingTableNTManager::configure(std::string beamProdPref, std::string SCEPrefix) {
    m_BeamProdPref = beamProdPref;
    m_SCENTOutPVName = SCEPrefix+SCENTOutPVNameSufix;
    m_ListEventsPVName = SCEPrefix+listEventsPVNameSufix;
    m_BeamModePVName = SCEPrefix+beamModePVNameSufix;
    m_BeamLength["value"] = 0;
    m_BeamLengthSp["value"] = 0;
    m_BeamPos["value"] = 0;
    m_BeamPosSp["value"] = 0;
    m_RfPiezoStDelaySp["value"] = 0.0;
    m_BPulseStPrevDelaySp["value"] = 0.0;
    m_ReloadTbl["value"] = 0;

    // Set initial value for labels so save and restore can "see" the PV
    // and have the right labels (otherwise it will identify as NOT EQUAL)
    pvxs::shared_array<std::string> labelsEmpty(maxCols); 
    m_PvTimTbl["labels"] = labelsEmpty.freeze();

    m_TimTblPv.onPut([this](pvxs::server::SharedPV& pv,
                        std::unique_ptr<pvxs::server::ExecOp>&& op,
                        pvxs::Value&& top) {
        // set timestamp
        epicsTimeStamp now;
        auto ts(top["timeStamp"]);
        if (!epicsTimeGetCurrent(&now)) {
            ts["secondsPastEpoch"] =
                now.secPastEpoch + POSIX_TIME_AT_EPICS_EPOCH;
            ts["nanoseconds"] = now.nsec;
        }
        // update SharedPV cache
        pv.post(top);
        // Store the put value on the SharedPv, so it can be used for reload
        m_PvTimTbl = top;

        // Configure client using $EPICS_PVA_*
        auto ctxt(pvxs::client::Context::fromEnv());

        // Get the RefTab Events from Supercycle IOC
        pvxs::Value listEvents = ctxt.get(m_ListEventsPVName).exec()->wait(5.0);
    
        prepareTable(top, listEvents);

        m_BeamLength["value"] = m_OutTable["metadata"]["BPulseLength"].as<const uint32_t>();
        // update SharedPV cache
        m_BeamLengthPv.post(m_BeamLength);

        //Update Beam Pulse Position
        m_BeamPosPv.post(m_BeamPos);

        // Update SCE Table
        updateSCETable();
      
        // Inform client tha the PUT operation is complete
        op->reply();

    });

    // Redefine onPut for Beam Lenght
    m_BeamLengthPvSp.onPut([this](pvxs::server::SharedPV& pv,
                        std::unique_ptr<pvxs::server::ExecOp>&& op,
                        pvxs::Value&& top) {
        // update SharedPV cache
        pv.post(top);
        // Only change Beam Pulse Length if all Beam Pulses have the same length
        // TODO:  Add some alarm if not allowed
        if (!m_AllowChangeBPulseLen){
            op->reply();
            return;
        }
        float beamLength = top["value"].as<float>();
        // when changing the length, the beam position should be the same
        float beamPos = m_BeamPos["value"].as<float>();

        changeBeam(beamLength, beamPos);

        op->reply();
    });


    // Redefine onPut for RfPiezoStDelaySp
    // Currently the change on this PV will not recalculate the table
    // the value will be used only for the next load table
    m_RfPiezoStDelayPvSp.onPut([this](pvxs::server::SharedPV& pv,
                        std::unique_ptr<pvxs::server::ExecOp>&& op,
                        pvxs::Value&& top) {
        // update SharedPV cache
        pv.post(top);

        m_RfPiezoStDelaySp["value"] = top["value"].as<float>();

        op->reply();
    });

    // Redefine onPut for BPulseStPrevDelaySp
    // Currently the change on this PV will not recalculate the table
    // the value will be used only for the next load table
    m_BPulseStPrevDelayPvSp.onPut([this](pvxs::server::SharedPV& pv,
                        std::unique_ptr<pvxs::server::ExecOp>&& op,
                        pvxs::Value&& top) {
        // update SharedPV cache
        pv.post(top);

        m_BPulseStPrevDelaySp["value"] = top["value"].as<float>();

        op->reply();
    });

    // Redefine onPut for m_BeamPosPvSp
    m_BeamPosPvSp.onPut([this](pvxs::server::SharedPV& pv,
                        std::unique_ptr<pvxs::server::ExecOp>&& op,
                        pvxs::Value&& top) {
        // update SharedPV cache
        pv.post(top);

        // Only change Beam Pulse poisition if all Beam Pulses have the same length
        // TODO:  Add some alarm if not allowed
        if (!m_AllowChangeBPulseLen){
            op->reply();
            return;
        }
        float beamPos = top["value"].as<float>();
        // when changing the poistion, the beam length should be the same
        float beamLength = m_BeamLength["value"].as<float>();

        changeBeam(beamLength, beamPos);

        op->reply();
    });

    // Redefine onPut for m_ReloadTblPv
    m_ReloadTblPv.onPut([this](pvxs::server::SharedPV& pv,
                        std::unique_ptr<pvxs::server::ExecOp>&& op,
                        pvxs::Value&& top) {
        // update SharedPV cache
        pv.post(top);

        reloadTable();

        op->reply();
    });

    m_TimTblPv.open(m_PvTimTbl);
    m_BeamLengthPv.open(m_BeamLength);
    m_BeamLengthPvSp.open(m_BeamLengthSp);
    m_BeamPosPv.open(m_BeamPos);
    m_BeamPosPvSp.open(m_BeamPosSp);
    m_RfPiezoStDelayPvSp.open(m_RfPiezoStDelaySp);
    m_BPulseStPrevDelayPvSp.open(m_BPulseStPrevDelaySp);
    m_ReloadTblPv.open(m_ReloadTbl);
    pvxs::ioc::server().addPV(m_BeamProdPref + NTTablePVNameSufix, m_TimTblPv);
    // These PVs has # on the name to indicate they are internal
    // They will be mapped on "standard PVs on the database"
    pvxs::ioc::server().addPV(m_BeamProdPref + "#"+ beamLengthPVNameSufix +"-RB", m_BeamLengthPv);
    pvxs::ioc::server().addPV(m_BeamProdPref + "#"+ beamLengthPVNameSufix +"-SP", m_BeamLengthPvSp);
    pvxs::ioc::server().addPV(m_BeamProdPref + "#"+ beamPosPVNameSufix +"-RB", m_BeamPosPv);
    pvxs::ioc::server().addPV(m_BeamProdPref + "#"+ beamPosPVNameSufix +"-SP", m_BeamPosPvSp);
    pvxs::ioc::server().addPV(m_BeamProdPref + "#"+ rfPiezoStDelayPVNameSufix +"-SP", m_RfPiezoStDelayPvSp);
    pvxs::ioc::server().addPV(m_BeamProdPref + "#"+ bPulseStPrevDelayPVNameSufix +"-SP", m_BPulseStPrevDelayPvSp);
    pvxs::ioc::server().addPV(m_BeamProdPref + "#"+ reloadTblPVNameSufix, m_ReloadTblPv);
}

// TODO: Maybe move this to another file

DEFINE_LOGGER(app, "BPIOC");

// routine executed at the IOC exit (iocHooks)
void TTAtExit(void *unused) { std::cout << "IOC finished" << std::endl; }

void TTCreatePV(std::string beamProdPref, std::string SCEPrefix) {
    TimingTableNTManager::manager().configure(beamProdPref, SCEPrefix);
}

//-----------------------------------------------------------------------------
// initHook callback
//-----------------------------------------------------------------------------
static void TTInitHook(initHookState state) {
    if ((state != initHookAfterIocBuilt) && (state != initHookAfterIocRunning))
        return;

    // early in the process, just setuo log and register Exit hook
    if (state == initHookAfterIocBuilt) {
        // register PV AT EXIT
        epicsAtExit(&TTAtExit, nullptr);

        pvxs::logger_level_set(app.name, pvxs::Level::Debug);
        pvxs::logger_config_env();
    }

    if (state == initHookAfterIocRunning) {
    }
}

extern "C" {

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// EPICS iocsh shell commands
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// IOC-shell command "TTCreatePV"
//-----------------------------------------------------------------------------
static const iocshArg config_Arg0{"beam_production_prefix", iocshArgString};
static const iocshArg config_Arg1{"sce_prefix", iocshArgString};
static const iocshArg *const config_Args[]{&config_Arg0, &config_Arg1};

static const iocshFuncDef config_FuncDef{
    "TTCreatePV", sizeof(config_Args) / sizeof(iocshArg *), config_Args};

static void config_CallFunc(const iocshArgBuf *args) {
    TTCreatePV(args[0].sval,args[1].sval);
}

static void TTRegistrar() {
    initHookRegister(&TTInitHook);
    iocshRegister(&config_FuncDef, config_CallFunc);
}

epicsExportRegistrar(TTRegistrar);
}  // extern C
