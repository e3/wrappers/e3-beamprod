#ifndef TIMINGTABLENT_H
#define TIMINGTABLENT_H

#include <pvxs/nt.h>
#include <pvxs/server.h>
#include <pvxs/sharedpv.h>
#include <pvxs/iochooks.h>
#include <pvxs/client.h>
#include <epicsTime.h>

//Maximum number of cols - it sets for 99 as the ID column shold be add
//SuperCycle Editor does not generate the ID columnd
constexpr size_t maxCols = 99;
constexpr size_t nominalNumberOfCycles = 14;
constexpr float beamWindowSt = 6500; //us
constexpr float beamWindowEnd = 9500; //us
#define SCENTOutPVNameSufix "ScTabNt-SP"
#define listEventsPVNameSufix "RefEvents-RB"
#define beamModePVNameSufix "BMod-Sel"
#define NTTablePVNameSufix "ScTabNt-SP"
#define beamLengthPVNameSufix "BeamPulseLength"
#define beamPosPVNameSufix "BeamPulsePos"
#define rfPiezoStDelayPVNameSufix "RFPiezoStDelay"
#define bPulseStPrevDelayPVNameSufix "BPulseStPrevDelay"
#define reloadTblPVNameSufix "ReloadTbl"

struct TimTblData {
    //! Type of the ".value" field.
    pvxs::TypeCode value;

    constexpr TimTblData() : value() {}

    //! A TypeDef which can be appended
    PVXS_API
    pvxs::TypeDef build(size_t size) const;

    //! Instantiate
    inline pvxs::Value create(size_t size) const { return build(size).create(); }
};

pvxs::TypeDef TimTblData::build(size_t size) const {
    using namespace pvxs::members;

    auto time_t(pvxs::nt::TimeStamp {}.build());
    auto alarm_t = {Int32("severity"), Int32("status"), String("message"), };

    pvxs::TypeDef def(
        pvxs::TypeCode::Struct, "epics:nt/NTTable:1.0",
        {StringA("labels"),
         Struct("metadata",
                {UInt32("BPulseLengthSrc"), UInt32("BPulseLengthLebt"),
                 UInt32("BPulseLength"),    Float32("BPulseFrequency"),
                 String("Comment"),         StringA("Keywords")}),
         time_t.as("timeStamp"), Struct("alarm", "alarm_t", alarm_t)});

    std::string fieldName;
    // adding the events arrays
    for (size_t i = 0; i < size; i++) {
        fieldName = "Col" + std::to_string(i);
        def += {Struct("value", {Float32A(fieldName)})};
    }
    return def;
}


class TimingTableNTManager  {
   public:
    // Meyers Singleton
    static TimingTableNTManager& manager() {
        static TimingTableNTManager instance;
        return instance;
    }

    void prepareTable(pvxs::Value supercycleIn, pvxs::Value listEvents);
    void updateSCETable();
    void changeBeam(float beamLength, float beamPos);
    void configure(std::string beamProdPref, std::string SCEPrefix);

    TimingTableNTManager()
        : m_PvTimTbl(TimTblData {}.create(maxCols)),
          m_TimTblPv(pvxs::server::SharedPV::buildMailbox()),
          m_BeamLengthPv(pvxs::server::SharedPV::buildMailbox()),
          m_BeamLengthPvSp(pvxs::server::SharedPV::buildMailbox()),
          m_BeamPosPv(pvxs::server::SharedPV::buildMailbox()),
          m_BeamPosPvSp(pvxs::server::SharedPV::buildMailbox()),
          m_RfPiezoStDelayPvSp(pvxs::server::SharedPV::buildMailbox()),
          m_BPulseStPrevDelayPvSp(pvxs::server::SharedPV::buildMailbox()),
          m_ReloadTblPv(pvxs::server::SharedPV::buildMailbox()),
          m_OutTable(TimTblData{}.create(maxCols+1)),
          m_BeamLength(pvxs::nt::NTScalar{pvxs::TypeCode::UInt32}.create()),
          m_BeamLengthSp(pvxs::nt::NTScalar{pvxs::TypeCode::UInt32}.create()),
          m_BeamPos(pvxs::nt::NTScalar{pvxs::TypeCode::UInt32}.create()),
          m_BeamPosSp(pvxs::nt::NTScalar{pvxs::TypeCode::UInt32}.create()),
          m_RfPiezoStDelaySp(pvxs::nt::NTScalar{pvxs::TypeCode::Float32}.create()),
          m_BPulseStPrevDelaySp(pvxs::nt::NTScalar{pvxs::TypeCode::Float32}.create()),
          m_ReloadTbl(pvxs::nt::NTScalar{pvxs::TypeCode::UInt32}.create()),
          m_ColLen(0),
          m_AllowChangeBPulseLen(false)
          {}

    ~TimingTableNTManager() = default;

   private:
    pvxs::Value m_PvTimTbl;
    pvxs::server::SharedPV m_TimTblPv;
    pvxs::server::SharedPV m_BeamLengthPv;
    pvxs::server::SharedPV m_BeamLengthPvSp;
    pvxs::server::SharedPV m_BeamPosPv;
    pvxs::server::SharedPV m_BeamPosPvSp;
    pvxs::server::SharedPV m_RfPiezoStDelayPvSp;
    pvxs::server::SharedPV m_BPulseStPrevDelayPvSp;
    pvxs::server::SharedPV m_ReloadTblPv;
    pvxs::Value m_OutTable;
    pvxs::Value m_BeamLength;
    pvxs::Value m_BeamLengthSp;
    pvxs::Value m_BeamPos;
    pvxs::Value m_BeamPosSp;
    pvxs::Value m_RfPiezoStDelaySp;
    pvxs::Value m_BPulseStPrevDelaySp;
    pvxs::Value m_ReloadTbl;
    //Position of every event on the NTTable
    std::map <std::string, int> m_EventIdxDict;
    size_t m_ColLen;
    bool m_AllowChangeBPulseLen;
    std::vector<std::string> m_EventNames;
    std::string m_BeamProdPref, m_SCENTOutPVName, m_ListEventsPVName, m_BeamModePVName;

    //Return the event position (in time), from the NTTable for a specific line
    float getEventPosition(pvxs::Value table, std::string eventName, size_t line, bool inputSC);
    void reloadTable();
};

#endif // TIMINGTABLENT_H
