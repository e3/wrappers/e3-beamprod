#### C++ static files
To refresh the static imports (`essLinacConfig.h`) based on the external git repository [https://gitlab.esss.lu.se/icshwi/reftabs](https://gitlab.esss.lu.se/icshwi/reftabs) use:
```bash
python beamprod/beamprodApp/src/src2template.py
```
>NOTE: **the updated C++ header should be part** of the committed repository

\```python:essLinacConfig.h

\```