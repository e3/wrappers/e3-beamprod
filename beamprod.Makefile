#
#  Copyright (c) 2019 - 2021, European Spallation Source ERIC
#
#  The program is free software: you can redistribute it and/or modify it
#  under the terms of the BSD 3-Clause license.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.
# 
# Author  : Saeed Haghtalab
# email   : saeed.haghtalab@ess.eu
# Date    : 2021-03-02
# version : 0.0.0 
#
# This template file is based on one generated by e3TemplateGenerator.bash.
# Please look at many other Makefile.E3 in the https://gitlab.esss.lu.se/epics-modules/ 
# repositories.
# 

## The following lines are mandatory, please don't change them.
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile


## Exclude archictetures
EXCLUDE_ARCHS += linux-ppc64e6500
EXCLUDE_ARCHS += linux-corei7-poky

############################################################################
#
# Relevant directories to point to files
#
############################################################################

APP:=beamprodApp
APPDB:=$(APP)/Db
APPSRC:=$(APP)/src


############################################################################
#
# Add any files that should be copied to $(module)/Db
#
############################################################################

TEMPLATES += $(wildcard $(APPDB)/*.template)


############################################################################
#
# Add any files that need to be compiled (e.g. .c, .cpp, .st, .stt)
#
############################################################################

SOURCES   += $(APPSRC)/setIrisForCurrentImpl.cpp
SOURCES   += $(APPSRC)/checkIfAllowed.cpp
SOURCES   += $(APPSRC)/beamParameters.cpp
SOURCES   += $(APPSRC)/timingtableNT.cpp


############################################################################
#
# Add any .dbd files that should be included (e.g. from user-defined functions, etc.)
#
############################################################################

DBDS   += $(APPSRC)/aSubFunctions.dbd
DBDS   += $(APPSRC)/timingtables.dbd

############################################################################
#
# Add any startup scripts that should be installed in the base directory
#
############################################################################

SCRIPTS += $(wildcard ../iocsh/*.iocsh)


.PHONY: db
db:

.PHONY: vlibs
vlibs:
