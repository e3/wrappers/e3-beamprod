# e3-beamprod: Operations Beam Production EPICS Module

This IOC acts as a proxy between the dedicated OPI and the SCE IOC.

[Check CHANGELOG to see various PVs that are exposed](CHANGELOG.md).

##  Timing tables checks and validations
tbc

# Building the IOC
## Code generation after definitions update

Prior to the IOC build one is encouraged to get the latest configuration included into the IOC.
A source of all configuration needed for this IOC is kept under the `reftabs` repository located under:
[https://gitlab.esss.lu.se/icshwi/reftabs](https://gitlab.esss.lu.se/icshwi/reftabs)

#### EPICS DB template
Part of the PVs in this IOC (:BState, :BMode and :BDestination) are automatically generated via python code + jinja template (all included). 
This is the case for this IOC and for the *TestEnviroment* (see below).

To get the EPICS db configured run:
```bash
python beamprod/beamprodApp/Db/beamprod-mode-dest.py
```
>NOTE: **the final EPICS DB file is part** of the committed repository

#### C++ static files
Also the c++ static code is generated based on the `reftabs` repository, to get it regenerated run:
```bash
python beamprod/beamprodApp/src/src2template.py
```
>NOTE: **the final C++ header file is part** of the committed repository

## Build

```bash
make cellinstall
iocsh.bash -l cellMods cmds/st.cmd
```

## Test Environment

To work locally with this IOC, one may enable simulated SCE IOC with the PVs exposed and used as in production.
To start working with the IOC including TEST dbs, enable the relevant files in `cmds/beamprod.cmd`)
>**NOTE**: lines including the TEST db files shall not be committed to the repository.
